/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Controlador;

/**
 *
 * @author sebas
 */
public class vdlgFecha extends javax.swing.JDialog {

    /**
     * Creates new form vdlgFecha
     */
    public vdlgFecha(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setSize(850,600);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDia = new javax.swing.JLabel();
        lblMes = new javax.swing.JLabel();
        lblAño = new javax.swing.JLabel();
        txtDia = new javax.swing.JTextField();
        txtMes = new javax.swing.JTextField();
        txtAño = new javax.swing.JTextField();
        btmMostrar = new javax.swing.JButton();
        btmGuardar = new javax.swing.JButton();
        btmLimpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        lblDia.setText("Dia");
        getContentPane().add(lblDia);
        lblDia.setBounds(60, 43, 18, 16);

        lblMes.setText("Mes");
        getContentPane().add(lblMes);
        lblMes.setBounds(60, 80, 23, 16);

        lblAño.setText("Año");
        getContentPane().add(lblAño);
        lblAño.setBounds(60, 120, 30, 16);
        getContentPane().add(txtDia);
        txtDia.setBounds(90, 40, 90, 22);
        getContentPane().add(txtMes);
        txtMes.setBounds(100, 80, 80, 22);
        getContentPane().add(txtAño);
        txtAño.setBounds(100, 120, 69, 22);

        btmMostrar.setText("Mostrar");
        getContentPane().add(btmMostrar);
        btmMostrar.setBounds(650, 20, 77, 25);

        btmGuardar.setText("Guardar");
        getContentPane().add(btmGuardar);
        btmGuardar.setBounds(650, 70, 79, 25);

        btmLimpiar.setText("Limpiar");
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(650, 120, 75, 25);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(vdlgFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(vdlgFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(vdlgFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(vdlgFecha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                vdlgFecha dialog = new vdlgFecha(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btmGuardar;
    public javax.swing.JButton btmLimpiar;
    public javax.swing.JButton btmMostrar;
    private javax.swing.JLabel lblAño;
    private javax.swing.JLabel lblDia;
    private javax.swing.JLabel lblMes;
    public javax.swing.JTextField txtAño;
    public javax.swing.JTextField txtDia;
    public javax.swing.JTextField txtMes;
    // End of variables declaration//GEN-END:variables

}
