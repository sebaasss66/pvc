/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Frame;
import modelo.Fecha;
import vista.vdlgFecha;
import java.awt.event.ActionEvent;


import java.awt.event.ActionListener;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class Controlador  implements ActionListener {
    private Fecha hoy;
    private vdlgFecha vista;
    
    public Controlador(Fecha hoy, vdlgFecha vista){
        this.hoy = hoy;
        this.vista = vista;
        
        vista.btmGuardar.addActionListener(this);
        vista.btmLimpiar.addActionListener(this);
        vista.btmMostrar.addActionListener(this);
    }

    public Controlador() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void iniciarVista(){
        vista.setTitle("Fecha ");
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        
        if (e.getSource()==vista.btmGuardar){
        hoy.setDia(Integer.parseInt(vista.txtDia.getText()));
        hoy.setMes(Integer.parseInt(vista.txtMes.getText()));
        hoy.setAño(Integer.parseInt(vista.txtAño.getText()));
        }
        
        if(e.getSource()==vista.btmMostrar){
            System.out.println("en mostrar");
        vista.txtDia.setText(String.valueOf(hoy.getDia()));
        vista.txtMes.setText(String.valueOf(hoy.getMes()));
        vista.txtAño.setText(String.valueOf(hoy.getAño()));
        if(hoy.isBisiesto()) JOptionPane.showMessageDialog(vista, "es un año bisiesto");
        }
        
        if(e.getSource()==vista.btmLimpiar){
        vista.txtDia.setText("");
        vista.txtMes.setText("");
        vista.txtAño.setText("");
        }
        
    }
    
    public static void main(String[] args ){
        Fecha hoy = new Fecha();
        vdlgFecha vista = new vdlgFecha(new Jframe(),true);
        
        Controlador control = new Controlador(hoy,vista);
        control.iniciarVista();
    }

    private static class Jframe extends Frame {

        public Jframe() {
        }
    }
    
    
}
